package com.qaagility.controller;

import static org.junit.Assert.*;
import org.junit.Test;

public class CountTest {

@Test
public void testDivideWhenZero(){
    assertEquals ("Denominotor is Zero ",Integer.MAX_VALUE, new Count().divide(5,0));
}


@Test
public void testDivideWhenNonZero(){
    assertEquals ("Denominotor is non Zero ",5, new Count().divide(10,2));
} 

}
