package com.qaagility.controller;

import static org.junit.Assert.*;
import org.junit.Test;

public class AboutTest {

@Test
public void testDesc(){
   final String result = new About().desc();
   assertEquals("Description" ,"This application was copied from somewhere, sorry but I cannot give the details and the proper copyright notice. Please don't tell the police!" , result );
}
}
